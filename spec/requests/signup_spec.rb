require 'rails_helper'

RSpec.describe 'POST /signup', type: :request do
  let(:url) { '/api/signup' }
  let(:params) do
    {
      user: attributes_for(:user)
    }
  end

  context 'when user is unauthenticated' do
    before { post url, params: params }

    it 'returns 200' do
      expect(response.status).to eq 200
    end

    it 'returns a new user' do
      expect_json(name: params[:user][:name], email: params[:user][:email])
      expect(json_body.key?(:encrypted_password)).to be_falsey
    end
  end

  context 'when user already exists' do
    before do
      create(:user, email: params[:user][:email])
      post url, params: params
    end

    it 'returns bad request status' do
      expect(response.status).to eq 400
    end

    it 'returns validation errors' do
      expect(json_body[:error][:title]).to eq('Bad request')
    end
  end
end