require 'rails_helper'

RSpec.describe '/api/v1/categories', type: :request do
  let(:user) do
    create(:user)
  end

  let(:auth_token) do
    login_with_api(user)
    response.headers['Authorization']
  end

  let(:category_from_another_user) do
    create(:category)
  end

  describe 'GET /api/v1/categories INDEX' do
    context 'User not logged in' do
      it 'fails with 401' do
        get api_v1_categories_path

        expect(response).to have_http_status(401)
      end
    end

    it 'returns empty array when there are no categories' do
      get api_v1_categories_path, headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(200)
      expect(json_body).to be_empty
    end

    it 'returns list of categories for the user' do
      @categories = create_list(:category, 5, user: user)
      create_list(:subcategory, 3, category: @categories.last)

      get api_v1_categories_path, headers: { 'Authorization': auth_token }
      expect(json_body.length).to eq(@categories.length)

      @categories.each do |category|
        expect_json('?', id: category.id, name: category.name, icon: category.icon)
        category.subcategories.each do |sub|
          expect_json('?.subcategories.?', id: sub.id, name: sub.name, category_id: sub.category_id)
        end
      end
    end

    it 'returns only categories and subcategories with deletion_date = nil' do
      @category = create(:category, user: user)
      @subcategory = create(:subcategory, category: @category)
      @subcategory_deleted = create(:subcategory, category: @category, deletion_date: Date.today - 1.day)

      get api_v1_categories_path, headers: { 'Authorization': auth_token }
      expect(json_body.length).to eq(1)

      expect(json_body[0][:subcategories].length).to eq(1)
      expect_json('?.subcategories.?', id: @subcategory.id, name: @subcategory.name,
                                            category_id: @subcategory.category_id, deletion_date: nil)
    end
  end

  describe 'GET /api/v1/categories/:id SHOW' do
    let(:category) do
      create(:category, :with_subcategories, user: user)
    end

    context 'User not logged in' do
      it 'fails with 401' do
        get api_v1_category_path(id: category.id)

        expect(response).to have_http_status(401)
      end
    end

    it 'returns the category with id provided' do
      get api_v1_category_path(id: category.id), headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(200)
      expect_json(id: category.id, name: category.name, icon: category.icon)
      category.subcategories.each do |sub|
        expect_json('subcategories.?', id: sub.id, name: sub.name, category_id: sub.category_id)
      end
    end

    it 'doesnt return subcategories with deletion_date != nil' do
      cat = create(:category, user: user)
      @categories_valid = create_list(:subcategory, 5, category: cat)
      create(:subcategory, category: cat, deletion_date: Date.today - 1.day)

      get api_v1_category_path(id: cat.id), headers: { 'Authorization': auth_token }
      
      expect(response).to have_http_status(200)
      expect_json(id: cat.id, name: cat.name, icon: cat.icon)
      expect(json_body[:subcategories].length).to eq(@categories_valid.length)
      
      @categories_valid.each do |sub|
        expect_json('subcategories.?', id: sub.id, name: sub.name, category_id: sub.category_id)
      end
    end

    it 'returns 404 when it doesnt find category' do
      get api_v1_category_path(id: 0), headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(404)
    end

    it 'returns 404 when trying to fetch category from another user' do
      get api_v1_category_path(id: category_from_another_user.id), headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(404)
    end
  end

  describe 'POST /api/v1/categories CREATE' do
    context 'User not logged in' do
      it 'fails with 401' do
        post api_v1_categories_path, params: attributes_for(:category)

        expect(response).to have_http_status(401)
      end
    end

    it 'creates categories with valid attributes' do
      attributes = attributes_for(:category, user: nil).merge({
                                                                subcategories_attributes: [attributes_for(:subcategory)]
                                                              })

      expect do
        post api_v1_categories_path, params: attributes, headers: { 'Authorization': auth_token }
      end.to change { Category.count }.by(1)

      expect(response).to have_http_status(201)
      expect_json(name: attributes[:name], icon: attributes[:icon])
      expect_json('subcategories.?', name: attributes[:subcategories_attributes][0][:name])
    end

    it 'returns 422 for invalid attributes' do
      expect do
        post api_v1_categories_path, params: { name: nil }, headers: { 'Authorization': auth_token }
      end.to_not change { Category.count }

      expect(response).to have_http_status(422)
      expect(json_body.dig(:error, :detail)).to be_present
    end
  end

  describe 'PUT /api/v1/categories/:id UPDATE' do
    let(:category) do
      create(:category, user: user)
    end

    context 'User not logged in' do
      it 'fails with 401' do
        put api_v1_category_path(id: category), params: attributes_for(:category)

        expect(response).to have_http_status(401)
      end
    end

    it 'updates category with id provided' do
      new_name = 'Novo nome'
      put api_v1_category_path(id: category), params: { name: new_name }, headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(200)
      expect_json(id: category.id, name: new_name, icon: category.icon)
    end

    it 'returns 422 for invalid attributes' do
      put api_v1_category_path(id: category), params: { name: nil }, headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(422)
      expect(json_body.dig(:error, :detail)).to be_present
    end

    it 'returns 404 when category with id provided doent exist' do
      put api_v1_category_path(id: 0), params: { name: 'outro nome' }, headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(404)
    end

    it 'returns 404 when trying to update category from another user' do
      put api_v1_category_path(id: category_from_another_user.id), params: { name: 'outro nome' },
                                                                   headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(404)
    end
  end

  describe 'DELETE /api/v1/categories/:id DESTROY' do
    let(:category) do
      create(:category, user: user)
    end

    context 'User not logged in' do
      it 'fails with 401' do
        delete api_v1_category_path(id: category.id)

        expect(response).to have_http_status(401)
      end
    end

    context 'keep_history = false' do
      it 'deletes category with id provided' do
        cat = create(:category, user: user)
        expenses = create_list(:expense, 5, user: user, category: cat)

        expect do
          delete api_v1_category_path(id: cat.id), params: { keep_history: false },
                                                   headers: { 'Authorization': auth_token }
        end.to change { Category.count }.by(-1)

        expect(response).to have_http_status(200)
        expect(Expense.count).to eq(expenses.length)
      end
    end

    context 'keeps_history = true' do
      it 'sets delete date to today' do
        cat = create(:category, :with_subcategories, user: user)
        expenses = create_list(:expense, 3, user: user, category: cat)

        expect do
          delete api_v1_category_path(id: cat.id), params: { keep_history: true },
                                                   headers: { 'Authorization': auth_token }
        end.to_not change { Category.count }

        expect(response).to have_http_status(200)
        cat.reload
        expect(cat.deletion_date.to_date).to eq(Date.today)
        expect(cat.subcategories.first.deletion_date.to_date).to eq(Date.today)
        expect(Expense.count).to eq(expenses.count)
      end
    end

    it 'returns 404 when there is no category with id provided' do
      delete api_v1_category_path(id: 0), headers: { 'Authorization': auth_token }
      expect(response).to have_http_status(404)
    end

    it 'returns 404 when trying to delete category from another user' do
      cat = category_from_another_user
      expect do
        delete api_v1_category_path(id: cat.id), headers: { 'Authorization': auth_token }
      end.to_not change { Category.count }

      expect(response).to have_http_status(404)
    end
  end
end
