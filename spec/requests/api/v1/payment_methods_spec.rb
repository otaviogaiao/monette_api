require 'rails_helper'

RSpec.describe '/api/v1/payment_methods', type: :request do
  let(:user) do
    create(:user)
  end

  let(:auth_token) do
    login_with_api(user)
    response.headers['Authorization']
  end

  let(:payment_method_from_another_user) do
    create(:payment_method)
  end

  describe 'GET /api/v1/payment_methods INDEX' do
    before(:each) do
      create_list(:payment_method, 4) # creates payment_methods for other users
    end

    context 'User not logged in' do
      it 'fails with 401' do
        get api_v1_payment_methods_path

        expect(response).to have_http_status(401)
      end
    end

    context 'No payment methods created' do
      it 'returns empty array' do
        get api_v1_payment_methods_path, headers: { 'Authorization': auth_token }

        expect(response).to have_http_status(200)
        expect(json_body).to be_empty
      end
    end

    it 'returns payment_methods from current user' do
      payment_methods = create_list(:payment_method, 5, user_id: user.id)
      get api_v1_payment_methods_path, headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(200)
      payment_methods.each do |payment_method|
        expect_json('?', id: payment_method.id, name: payment_method.name, frequency: payment_method.frequency,
                    start_day: payment_method.start_day, frequency_label: PaymentMethod.human_attribute_name("frequency.#{payment_method.frequency}"))
      end
      expect(json_body.length).to eq(5)
    end
  end

  describe 'GET /api/v1/payment_methods/:id SHOW' do
    let(:payment_method) do
      create(:payment_method, user: user)
    end

    context 'User not logged in' do
      it 'fails with 401' do
        get api_v1_payment_method_path(id: payment_method.id)

        expect(response).to have_http_status(401)
      end
    end

    it 'returns 404 when trying to access payment_method from another user' do
      get api_v1_payment_method_path(id: payment_method_from_another_user.id), headers: {
        'Authorization': auth_token
      }

      expect(response).to have_http_status(404)
    end

    it 'returns 404 when it doesnt find payment_method' do
      get api_v1_payment_method_path(id: 0), headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(404)
    end

    it 'returns payment_method' do
      get api_v1_payment_method_path(id: payment_method.id), headers: { 'Authorization': auth_token }

      expect(response).to have_http_status(200)
      expect_json(id: payment_method.id, name: payment_method.name, frequency: payment_method.frequency,
                  start_day: payment_method.start_day,
                  frequency_label: PaymentMethod.human_attribute_name("frequency.#{payment_method.frequency}"))
    end
  end

  describe 'POST /api/v1/payment_methods CREATE' do
    context 'User not logged in' do
      it 'fails with 401' do
        expect do
          post api_v1_payment_methods_path, params: attributes_for(:payment_method, user: nil)
        end.to_not change { PaymentMethod.count }

        expect(response).to have_http_status(401)
      end
    end

    it 'creates new payment method' do
      attributes = attributes_for(:payment_method, user: nil)

      expect do
        post api_v1_payment_methods_path, params: attributes, headers: {
          'Authorization': auth_token
        }
      end.to change { PaymentMethod.count }.by(1)

      expect(response).to have_http_status(201)
      expect_json(name: attributes[:name], frequency: attributes[:frequency].to_s, start_day: attributes[:start_day],
                  frequency_label: PaymentMethod.human_attribute_name("frequency.#{attributes[:frequency]}"))

      expect(PaymentMethod.find(json_body[:id]).user_id).to eq(user.id)
    end

    it 'returns 422 when there are invalid attributes' do
      invalid_attributes = attributes_for(:payment_method, user: nil, start_day: 32)

      expect do
        post api_v1_payment_methods_path, params: invalid_attributes, headers: {
          'Authorization': auth_token
        }
      end.to_not change { PaymentMethod.count }

      expect(response).to have_http_status(422)
      expect(json_body.dig(:error, :detail)).to be_present
    end

    it 'limits to 50 payment_methods per user' do
      create_list(:payment_method, 50, user_id: user.id)

      expect do
        post api_v1_payment_methods_path, params: attributes_for(:payment_method, user: nil), headers: {
          'Authorization': auth_token
        }
      end.to_not change { PaymentMethod.count }

      expect(response).to have_http_status(400)
      expect(json_body.dig(:error, :detail)).to be_present
    end
  end

  describe 'PUT/PATCH /api/v1/payment_methods/:id UPDATE' do
    let(:payment_method) do
      create(:payment_method, user: user)
    end

    context 'User not logged in' do
      it 'fails with 401' do
        put api_v1_payment_method_path(id: payment_method.id), params: { start_day: 2 }

        expect(response).to have_http_status(401)
      end
    end

    it 'updates payment method' do
      put api_v1_payment_method_path(id: payment_method.id), params: { start_day: 3 }, headers: {
        'Authorization': auth_token
      }

      expect(response).to have_http_status(200)
      expect_json(name: payment_method.name, frequency: payment_method.frequency, start_day: 3,
                  frequency_label: PaymentMethod.human_attribute_name("frequency.#{payment_method.frequency}"))
    end

    it 'returns 404 when it doesnt find payment_method' do
      put api_v1_payment_method_path(id: 0), params: { start_day: 4 }, headers: {
        'Authorization': auth_token
      }

      expect(response).to have_http_status(404)
    end

    it 'returns 404 when trying to access payment_method from another user' do
      put api_v1_payment_method_path(id: payment_method_from_another_user.id), params: { name: 'NEW NAME' }, headers: {
        'Authorization': auth_token
      }

      expect(response).to have_http_status(404)
    end

    it 'returns 422 when there are invalid attributes' do
      put api_v1_payment_method_path(id: payment_method.id), params: { start_day: 33 }, headers: {
        'Authorization': auth_token
      }

      expect(response).to have_http_status(422)
      expect(json_body.dig(:error, :detail)).to be_present
    end
  end

  describe 'DELETE /api/v1/payment_methods/:id DESTROY' do
    let(:payment_method) do
      create(:payment_method, user: user)
    end

    context 'User not logged in' do
      it 'fails with 401' do
        delete api_v1_payment_method_path(id: payment_method.id)

        expect(response).to have_http_status(401)
      end
    end

    it 'returns 404 when it doesnt find payment_method' do
      delete api_v1_payment_method_path(id: 0), headers: {
        'Authorization': auth_token
      }

      expect(response).to have_http_status(404)
    end

    it 'returns 404 when trying to access payment_method from another user' do
      delete api_v1_payment_method_path(id: payment_method_from_another_user.id), headers: {
        'Authorization': auth_token
      }

      expect(response).to have_http_status(404)
    end

    it 'deletes payment method' do
      id = payment_method.id
      expect do
        delete api_v1_payment_method_path(id: id), headers: {
          'Authorization': auth_token
        }
      end.to change { PaymentMethod.count }.by(-1)

      expect(response).to have_http_status(200)
    end
  end
end
