require 'rails_helper'

RSpec.describe '/api/v1/periods_configurations', type: :request do
  let(:user) do
    create(:user)
  end

  let(:auth_token) do
    login_with_api(user)
    response.headers['Authorization']
  end

  before(:each) do
    create_list(:period_configuration, 5)
  end

  before(:each) do
    @period_configuration = create(:period_configuration, user: user)
  end

  describe 'GET /api/v1/periods_configurations (INDEX)' do
    context 'User logged in' do
      it 'returns the current configuration for the user' do
        get api_v1_period_configuration_path, headers: {
          'Authorization': auth_token
        }

        expect(response).to have_http_status(200)
        expect_json(id: @period_configuration.id, frequency: @period_configuration.frequency,
                    frequency_label: PeriodConfiguration.human_attribute_name("frequency.#{@period_configuration.frequency}"),
                    num_frequency: @period_configuration.num_frequency,
                    income_value_base: @period_configuration.income_value_base.to_f, start_day: @period_configuration.start_day)
      end
    end

    context 'User not logged in' do
      it 'returns 401' do
        get api_v1_period_configuration_path

        expect(response).to have_http_status(401)
      end
    end
  end

  describe 'PUT/PATCH /api/v1/periods_configurations (UPDATE)' do
    context 'User logged in' do
      it 'updates configuration for that user' do
        income_value_base = 2004.04
        expect do
          put api_v1_period_configuration_path, params: {
            frequency: 'weekly',
            num_frequency: 2,
            income_value_base: income_value_base,
            start_day: 2
          }, headers: { 'Authorization': auth_token }
        end.to_not change {
          PeriodConfiguration.count
        }

        expect(response).to have_http_status(200)
        expect_json(id: @period_configuration.id, frequency: 'weekly',
                    frequency_label: PeriodConfiguration.human_attribute_name('frequency.weekly'),
                    num_frequency: 2,
                    income_value_base: income_value_base, start_day: 2)
      end

      it 'returns error for invalid values' do
        expect do
          put api_v1_period_configuration_path, params: {
            frequency: 'weekly',
            num_frequency: 10,
            income_value_base: 9090,
            start_day: 10
          }, headers: { 'Authorization': auth_token }
        end.to_not change {
                     PeriodConfiguration.count
                   }

        expect(response).to have_http_status(422)
        expect(json_body.dig(:error, :detail)).to be_present
      end
    end

    context 'User not logged in' do
      it 'returns 401' do
        income_value_base = 2004.04
        put api_v1_period_configuration_path, params: {
          frequency: 'weekly',
          num_frequency: 2,
          income_value_base: income_value_base,
          start_day: 2
        }
        expect(response).to have_http_status(401)
      end
    end
  end
end
