require 'rails_helper'

RSpec.describe CreateCategory do
  let(:user) do
    create(:user)
  end

  context 'attributes are valid' do
    it 'creates new category' do
      attributes = attributes_for(:category, user: nil)
      attributes = attributes.merge({ subcategories_attributes: [attributes_for(:subcategory, category: nil)] })

      result = nil
      expect do
        result = CreateCategory.result(user_id: user.id, category_params: attributes)
      end.to change { Category.count }.by(1)

      expect(result.success?).to be_truthy
      expect(result.category.name).to eq(attributes[:name])
      expect(result.category.icon).to eq(attributes[:icon])
      expect(result.category.subcategories.count).to eq(1)
    end
  end

  context 'attributes are invalid' do
    it 'doesnt create category' do
        result = nil
        expect do
            result = CreateCategory.result(user_id: user.id, category_params: {})
        end.to_not change { Category.count }

        expect(result.success?).to be_falsey
        expect(result.category.valid?).to be_falsey
    end
  end
end
