require 'rails_helper'

RSpec.describe DeletePaymentMethod do
  before(:each) do
    @payment_method = create(:payment_method)
  end

  it 'deletes payment method' do
    result = nil
    expect do
        result = DeletePaymentMethod.result(payment_method: @payment_method)
    end.to change { PaymentMethod.count }.by(-1)

    expect(result).to be_truthy
  end
end
