require 'rails_helper'

RSpec.describe UpdatePaymentMethod do
  before(:each) do
    @payment_method = create(:payment_method)
  end

  context 'Attributes are valid' do
    it 'Updates payment method and successfully finishes' do
      result = UpdatePaymentMethod.result(payment_method: @payment_method,
                                           payment_method_params: { start_day: 3 })
      expect(result.success?).to be_truthy
      expect(result.payment_method.errors).to be_blank
    end
  end

  context 'Attributes are invalid' do
    it 'fails to update payment method' do
      result = UpdatePaymentMethod.result(payment_method: @payment_method,
                                          payment_method_params: { start_day: 42 })
      expect(result.success?).to be_falsey
      expect(result.payment_method.valid?).to be_falsey
      expect(result.payment_method.errors).to be_present
    end
  end
end
