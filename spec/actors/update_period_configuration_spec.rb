require 'rails_helper'

RSpec.describe UpdatePeriodConfiguration do
  before(:each) do
    @period_configuration = create(:period_configuration)
  end

  context 'Attributes are valid' do
    it 'Updates period configuration and successfully finishes' do
      context = UpdatePeriodConfiguration.result(period_configuration: @period_configuration,
                                                 period_configuration_params: { start_day: 3 })
      expect(context.success?).to be_truthy
      expect(@period_configuration.errors).to be_blank
    end
  end

  context 'Attributes are invalid' do
    it 'fails to update period configuration' do
      context = UpdatePeriodConfiguration.result(period_configuration: @period_configuration,
                                                 period_configuration_params: { start_day: 42 })
      expect(context.success?).to be_falsey
      expect(@period_configuration.valid?).to be_falsey
      expect(@period_configuration.errors).to be_present
    end
  end
end
