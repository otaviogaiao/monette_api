require 'rails_helper'

RSpec.describe CreatePaymentMethod do
  let(:user) do
    create(:user)
  end

  context 'Attributes are valid' do
    it 'Creates payment method and successfully finishes' do
      result = nil
      expect do
        result = CreatePaymentMethod.result(
          attributes_for(:payment_method, user: nil,
                                          payment_method_params: attributes_for(:payment_method, user: nil)), user_id: user.id
        )
      end.to change { PaymentMethod.count }.by(1)

      expect(result.success?).to be_truthy
      expect(result.payment_method.persisted?).to be_truthy
    end
  end

  context 'Attributes are invalid' do
    it 'fails to create payment method' do
      result = nil
      expect do
        result = CreatePaymentMethod.result(user_id: user.id,
                                            payment_method_params: { start_day: 42 })
      end.to_not change { PaymentMethod.count }

      expect(result.success?).to be_falsey
      expect(result.payment_method.valid?).to be_falsey
      expect(result.payment_method.errors).to be_present
    end
  end

  it 'It fails when already reached max number of payment methods' do
    create_list(:payment_method, 50, user_id: user.id)

    result = nil
    expect do
      result = CreatePaymentMethod.result(payment_method_params: attributes_for(:payment_method, user: nil),
                                          user_id: user.id)
    end.to_not change { PaymentMethod.count }

    expect(result.failure?).to be_truthy
    expect(result.errors).to be_present
    expect(result.status).to eq(400)
  end
end
