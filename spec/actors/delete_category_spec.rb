require 'rails_helper'

RSpec.describe DeleteCategory do
  let(:user) {
    create(:user)
  }

  context 'keep_history = true' do
    it 'sets deletion_date' do
      @category = create(:category, :with_subcategories, user: user)
      create_list(:expense, 3, category: @category, user: user)

      result = nil
      expect {
        result = DeleteCategory.result(category: @category, keep_history: true)
      }.to_not change { Category.count }
      
      expect(result.success?).to be_truthy
      expect(result.category.deletion_date).to be_present
      result.category.subcategories.each do |sub|
        expect(sub.deletion_date).to be_present
      end
    end
  end

  context 'keep_history = false' do
    it 'destroys category and subcategories' do
      @category = create(:category, :with_subcategories, user: user)
      create_list(:expense, 3, category: @category, user: user)

      result = nil
      expect {
        result = DeleteCategory.result(category: @category, keep_history: false)
      }.to change { Category.count }.by(-1)

      expect(result.success?).to be_truthy
    end 
  end
end
