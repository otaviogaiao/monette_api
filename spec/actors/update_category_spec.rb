require 'rails_helper'

RSpec.describe UpdateCategory do
  let(:user) do
    create(:user)
  end

  before(:each) do
    @category = create(:category, :with_subcategories, user: user)
  end

  context 'attributes are invalid' do
    it 'doesnt update category' do
      result = nil
      expect do
        result = UpdateCategory.result(user_id: user.id, category: @category,
                                       category_params: { name: nil, subcategories_attributes: [{ name: 'Valid' }] })
      end.to_not change { Subcategory.count }

      expect(result.success?).to be_falsey
      expect(@category.valid?).to be_falsey
    end
  end

  context 'attributes are valid' do
    it 'updates category' do
      result = nil
      new_name = 'New name'

      expect do
        result = UpdateCategory.result(user_id: user.id, category: @category,
                                       category_params: { name: new_name, subcategories_attributes: [{ name: 'Valid' }] })
      end.to change { Subcategory.count }.by(1)

      expect(result.category.name).to eq(new_name)
    end
  end
end
