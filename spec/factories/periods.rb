FactoryBot.define do
  factory :period do
    start_date { Faker::Date.in_date_period }
    end_date { start_date + 1.month }

    income_value  { Faker::Number.decimal(l_digits: 2, r_digits: 4) }

    association :user, factory: :user
  end
end
