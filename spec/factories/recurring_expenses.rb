FactoryBot.define do
  factory :recurring_expense do
    name { Faker::FunnyName.name }
    value { Faker::Number.decimal(l_digits: 2, r_digits: 4) }
    base_day { Faker::Number.between(from: 1, to: 28) }

    start_date { Faker::Date.in_date_period }
    end_date { nil }

    association :user, factory: :user
    association :category, factory: :category
    subcategory { create(:subcategory, category: category) } #association subcategory
    
    association :payment_method, factory: :payment_method

    trait :with_end_date do
      end_date { start_date + 1.year }
    end
  end
end
