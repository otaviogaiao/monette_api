FactoryBot.define do
  factory :period_configuration do
    frequency { :monthly }
    num_frequency { 1 }

    income_value_base { Faker::Number.decimal(l_digits: 2, r_digits: 4) }
    start_day { 1 }

    association :user, factory: :user
  end
end
