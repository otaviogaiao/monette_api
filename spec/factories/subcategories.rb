FactoryBot.define do
  factory :subcategory do
    name { Faker::Computer.stack }
    deletion_date { nil }
    
    association :category, factory: :category

    trait :deleted do
      deletion_date { Faker::Date.backwards(days: 30)}
    end
  end
end
