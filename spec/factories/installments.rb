FactoryBot.define do
  factory :installment do
    name { Faker::Nation.capital_city }
    category { nil }
    subcategory { nil }

    total_value { Faker::Number.decimal(l_digits: 2, r_digits: 4) }
    date_first_installment { Faker::Date.in_date_period }
    number_installments { Faker::Number.within(range: 1..10) }
    interest_rate { 0 }

    association :user, factory: :user
  end
end
