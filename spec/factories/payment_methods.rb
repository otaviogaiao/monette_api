FactoryBot.define do
  factory :payment_method do
    name { Faker::Alphanumeric.alphanumeric(number: 10) }
    frequency { :monthly }
    start_day { 1 }

    association :user, factory: :user
  end
end
