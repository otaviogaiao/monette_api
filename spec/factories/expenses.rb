FactoryBot.define do
  factory :expense do
    name { Faker::Hipster.word }
    value { Faker::Number.decimal(l_digits: 2, r_digits: 4) }
    date_ocurred { Faker::Date.in_date_period }

    period { nil }
    recurring_expense { nil }
    payment_method { nil }
    installment { nil }
    category { nil }
    subcategory { nil }

    association :user, factory: :user

    trait :with_associations do
      user { create(:user) }
      period { create(:period, user: user) }
      payment_method { create(:payment_method, user: user) }

      category { create(:category, user: user) }
      subcategory { create(:subcategory, user: user, category: category) }
    end
  end
end
