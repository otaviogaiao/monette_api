FactoryBot.define do
  factory :category do
    name { Faker::ProgrammingLanguage.name }
    icon { nil }

    deletion_date { nil }

    association :user, factory: :user

    trait :with_deletion_date do
      deletion_date { Faker::Date.backward(days: 3) }
    end

    trait :with_subcategories do
      after(:create) do |category|
        create_list(:subcategory, 3, category: category)
      end
    end
  end
end
