FactoryBot.define do
  factory :spending_target do
    value_pct { Faker::Number.decimal(l_digits: 3, r_digits: 2) }

    association :category, factory: :category
    association :period, factory: :period
  end
end
