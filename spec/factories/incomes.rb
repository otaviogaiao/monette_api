FactoryBot.define do
  factory :income do
    name { Faker::Ancient.god }
    date_ocurred { Faker::Date.between(from: period.start_date, to: period.end_date) }
    value { Faker::Number.decimal(l_digits: 2, r_digits: 4) }

    association :period, factory: :period
    association :user, factory: :user
  end
end
