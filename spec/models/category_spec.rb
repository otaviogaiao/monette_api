require 'rails_helper'

RSpec.describe Category, type: :model do

  it 'contains paper_trail' do
    is_expected.to be_versioned
  end

  context 'associations' do
    it { should belong_to(:user).with_foreign_key(:user_id) }
    it { should have_many(:subcategories).with_foreign_key(:category_id).dependent(:destroy) }
    it { should have_many(:spending_targets).with_foreign_key(:category_id).dependent(:destroy) }
    it { should have_many(:recurring_expenses).with_foreign_key(:category_id).dependent(:nullify) }
    it { should have_many(:installments).with_foreign_key(:category_id).dependent(:nullify) }
    it { should have_many(:expenses).with_foreign_key(:category_id).dependent(:nullify) }

    it { should have_one(:spending_target_configuration).with_foreign_key(:category_id).dependent(:destroy)}

    it { should accept_nested_attributes_for(:subcategories) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
  end

  context 'callbacks' do
    describe 'update_deletion_date' do
      it 'sets deletion_date if remove is true' do
        category = create(:category)

        expect(category.deletion_date).to be_blank
        category.remove = true
        category.save

        expect(category.deletion_date).to be_present
      end
    end
  end
end
