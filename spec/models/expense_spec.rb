require 'rails_helper'

RSpec.describe Expense, type: :model do

  it 'contains paper_trail' do
    is_expected.to be_versioned
  end
  
  context 'associations' do
    it { should belong_to(:user).with_foreign_key(:user_id) }
    it { should belong_to(:period).with_foreign_key(:period_id).required(false) }
    it { should belong_to(:category).with_foreign_key(:category_id).required(false) }
    it { should belong_to(:subcategory).with_foreign_key(:subcategory_id).required(false) }
    it { should belong_to(:recurring_expense).with_foreign_key(:recurring_expense_id).required(false) }
    it { should belong_to(:payment_method).with_foreign_key(:payment_method_id).required(false) }
    it { should belong_to(:installment).with_foreign_key(:installment_id).required(false) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:value) }
    it { should validate_presence_of(:date_ocurred) }

    it 'cant have both installment and recurring_expense set' do
      expense = build(:expense, recurring_expense: create(:recurring_expense), installment: create(:installment))

      expect(expense.valid?).to be_falsey
      expect(expense.errors.key?(:base)).to be_truthy
    end

    context 'subcategory is set' do
      let(:subcategory) do
        create(:subcategory)
      end

      it 'should have category set' do
        expense = build(:expense, category: nil, subcategory: subcategory)

        expect(expense.save).to be_falsey
        expect(expense.errors.key?(:category)).to be_truthy
      end

      it 'should have category of the subcategory equals to category' do
        expense = build(:expense, subcategory: subcategory)

        expect(expense.save).to be_falsey
        expect(expense.errors.key?(:category)).to be_truthy
      end
    end

    it 'doesnt allow date outside of period date range' do
      user = create(:user)
      period = create(:period, user: user, start_date: DateTime.current, end_date: DateTime.current + 1.month)

      expense = build(:expense, user: user, period: period, date_ocurred: period.start_date - 2.days)

      expect(expense.valid?).to be_falsey
      expect(expense.save).to be_falsey
      expect(expense.errors.key?(:date_ocurred)).to be_truthy
    end
  end
end
