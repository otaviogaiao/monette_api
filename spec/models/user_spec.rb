require 'rails_helper'

RSpec.describe User, type: :model do
  subject do
    build(:user)
  end

  it 'contains paper_trail' do
    is_expected.to be_versioned
  end
  
  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email).case_insensitive }
  end

  context 'associations' do
    it { should have_many(:categories).with_foreign_key(:user_id).dependent(:destroy) }
    it { should have_many(:periods_configurations).with_foreign_key(:user_id).dependent(:destroy) }
    it { should have_many(:periods).with_foreign_key(:user_id).dependent(:destroy) }
    it { should have_many(:incomes).with_foreign_key(:user_id).dependent(:destroy) }
    it { should have_many(:recurring_expenses).with_foreign_key(:user_id).dependent(:destroy)}
    it { should have_many(:payment_methods).with_foreign_key(:user_id).dependent(:destroy)}
    it { should have_many(:installments).with_foreign_key(:user_id).dependent(:destroy)}
    it { should have_many(:expenses).with_foreign_key(:user_id).dependent(:destroy)}
  end
end
