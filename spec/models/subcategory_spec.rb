require 'rails_helper'

RSpec.describe Subcategory, type: :model do
  it 'contains paper_trail' do
    is_expected.to be_versioned
  end
  
  context 'associations' do
    it { should belong_to(:category).with_foreign_key(:category_id) }
    it { should have_many(:recurring_expenses).with_foreign_key(:subcategory_id) }
    it { should have_many(:installments).with_foreign_key(:subcategory_id) }
    it { should have_many(:expenses).with_foreign_key(:subcategory_id) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
  end

  context 'callbacks' do
    describe 'update_deletion_date' do
      it 'sets deletion_date if remove is true' do
        subcategory = create(:subcategory)

        expect(subcategory.deletion_date).to be_blank
        subcategory.remove = true
        subcategory.save

        expect(subcategory.deletion_date).to be_present
      end
    end
  end
end
