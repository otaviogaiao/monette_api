require 'rails_helper'

RSpec.describe PaymentMethod, type: :model do
  it 'contains paper_trail' do
    is_expected.to be_versioned
  end

  it {
    should define_enum_for(:frequency)
      .with_values(weekly: 1, monthly: 2)
  }

  context 'associations' do
    it { should belong_to(:user).with_foreign_key(:user_id) }

    it { should have_many(:expenses).with_foreign_key(:payment_method_id).dependent(:nullify) }
  end

  context 'validations' do
    subject do
      create(:payment_method)
    end

    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name).scoped_to(:user_id) }

    it 'should not save for invalid start_day' do
      invalid = build(:payment_method, frequency: :monthly, start_day: 0, user: create(:user))

      expect(invalid.valid?).to be_falsey

      invalid = build(:payment_method, frequency: :weekly, start_day: 0, user: create(:user))

      expect(invalid.valid?).to be_falsey
    end
  end
end
