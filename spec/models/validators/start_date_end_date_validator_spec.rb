require 'rails_helper'

RSpec.describe StartDateEndDateValidator do
  with_model :invalid_model do
    table do |t|
      t.string :name
    end

    model do
      validates_with ::StartDateEndDateValidator
    end
  end

  with_model :valid_model do
    table do |t|
      t.date :start_date
      t.date :end_date
    end

    model do
      validates_with ::StartDateEndDateValidator
    end
  end

  context "class doesn't have start_date or end_date attributes" do
    it 'raises an exception' do
      expect { InvalidModel.new(name: 'This is an invalid model').valid? }.to raise_error(StandardError)
    end
  end

  context 'Start date is before End date' do
    it 'record is valid' do
      expect(ValidModel.new(start_date: Date.today, end_date: Date.today + 2.days).valid?).to be_truthy
    end
  end

  context 'Start date is after end date' do
    it 'record is invalid' do
      expect(ValidModel.new(start_date: Date.today, end_date: Date.today - 2.days).valid?).to be_falsey
    end
  end
end
