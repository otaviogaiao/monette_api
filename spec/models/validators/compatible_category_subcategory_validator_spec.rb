require 'rails_helper'

RSpec.describe CompatibleCategorySubcategoryValidator do
  with_model :invalid_model do
    table do |t|
      t.string :name
    end

    model do
      validates_with ::CompatibleCategorySubcategoryValidator
    end
  end

  with_model :valid_model do
    table do |t|
      t.belongs_to :category
      t.belongs_to :subcategory
    end

    model do
      belongs_to :category
      belongs_to :subcategory
      validates_with ::CompatibleCategorySubcategoryValidator
    end
  end

  context "class doesn't have category or subcategory associations" do
    it 'raises an exception' do
      expect { InvalidModel.new(name: 'This is an invalid model').valid? }.to raise_error(StandardError)
    end
  end

  context 'category is diferent from the category of the subcategory' do
    it 'model is invalid' do
      expect(ValidModel.new(category: create(:category), subcategory: create(:subcategory)).valid?).to be_falsey
    end
  end

  context 'category is equal to the category of the subcategory' do
    it 'model is valid' do
      cat = create(:category)
      subcat = create(:subcategory, category: cat)
      expect(ValidModel.new(category: cat, subcategory: subcat).valid?).to be_truthy
    end
  end
end
