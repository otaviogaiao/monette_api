require 'rails_helper'

RSpec.describe StartDayIsValidForFrequencyValidator do
  with_model :model_with_no_frequency do
    table do |t|
      t.string :nome
      t.integer :start_day
    end

    model do
      validates_with ::StartDayIsValidForFrequencyValidator
    end
  end

  with_model :model_with_no_start_day do
    table do |t|
      t.string :nome
      t.integer :frequency
    end

    model do
      validates_with ::StartDayIsValidForFrequencyValidator
      enum frequency: { weekly: 1, monthly: 2 }
    end
  end

  with_model :valid_model do
    table do |t|
      t.string :nome
      t.integer :start_day
      t.integer :frequency
    end

    model do
      validates_with ::StartDayIsValidForFrequencyValidator
      enum frequency: { weekly: 1, monthly: 2 }
    end
  end

  it 'raises exception for model with no frequency' do
    expect { ModelWithNoFrequency.new.valid? }.to raise_error(StandardError)
  end

  it 'raises exception for model with no start_day' do
    expect { ModelWithNoStartDay.new.valid? }.to raise_error(StandardError)
  end

  it 'adds error for invalid start_day for weekly model' do
    valid_model = ValidModel.new(frequency: :weekly, start_day: 0)

    expect(valid_model.valid?).to be_falsey

    valid_model = ValidModel.new(frequency: :weekly, start_day: 8)

    expect(valid_model.valid?).to be_falsey
  end

  it 'adds error for invalid start_day for monthly model' do
    valid_model = ValidModel.new(frequency: :monthly, start_day: 0)

    expect(valid_model.valid?).to be_falsey

    valid_model = ValidModel.new(frequency: :monthly, start_day: 32)

    expect(valid_model.valid?).to be_falsey
  end

  it 'doesnt add error for valid values' do
    valid_model = ValidModel.new(frequency: :monthly, start_day: 1)

    expect(valid_model.valid?).to be_truthy

    valid_model = ValidModel.new(frequency: :weekly, start_day: 1)

    expect(valid_model.valid?).to be_truthy
  end
end
