require 'rails_helper'

RSpec.describe DateInPeriodValidator do
  with_model :invalid_model do
    table do |t|
      t.datetime :date_ocurred
    end

    model do
      validates :date_ocurred, date_in_period: true
    end
  end

  with_model :valid_model do
    table do |t|
      t.datetime :date_ocurred
      t.belongs_to :period
    end

    model do
      belongs_to :period

      validates :date_ocurred, date_in_period: true
    end
  end

  context 'Record doest have period association' do
    it 'raises exception' do
      expect { InvalidModel.new(date_ocurred: DateTime.current).valid? }.to raise_error(StandardError)
    end
  end

  let(:period) do
    create(:period, start_date: DateTime.current - 5.days, end_date: DateTime.current + 5.days)
  end

  context 'value is out of period date range' do
    it 'turns record invalid' do
      expect(ValidModel.new(date_ocurred: period.end_date + 2.days, period: period).valid?).to be_falsey
      expect(ValidModel.new(date_ocurred: period.start_date - 2.days, period: period).valid?).to be_falsey
    end
  end

  context 'value is within period date range' do
    it 'allows record to be valid' do
      expect(ValidModel.new(date_ocurred: period.end_date - 1.days, period: period).valid?).to be_truthy
      expect(ValidModel.new(date_ocurred: period.end_date, period: period).valid?).to be_truthy
      expect(ValidModel.new(date_ocurred: period.start_date, period: period).valid?).to be_truthy
      expect(ValidModel.new(date_ocurred: period.start_date + 1.days, period: period).valid?).to be_truthy
    end
  end
end
