require 'rails_helper'

RSpec.describe Period, type: :model do

  it 'contains paper_trail' do
    is_expected.to be_versioned
  end
  
  context 'associations' do
    it { should belong_to(:user).with_foreign_key(:user_id) }
    it { should have_many(:incomes).with_foreign_key(:period_id) }
    it { should have_many(:expenses).with_foreign_key(:period_id)}
  end

  context 'validations' do
    it { should validate_presence_of(:start_date) }
    it { should validate_presence_of(:end_date) }
    it { should validate_presence_of(:income_value) }

    it 'should not allow start_date > end_date' do
      period = build(:period, user: create(:user), start_date: Date.today, end_date: Date.today - 2.days)

      salvo = period.save 
      expect(salvo).to be_falsey
      expect(period.errors.key?(:end_date)).to be_truthy
    end

    it 'saves' do
      period = create(:period)

      expect(period.persisted?).to be_truthy
    end
  end
end
