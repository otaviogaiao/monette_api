require 'rails_helper'

RSpec.describe SpendingTargetConfiguration, type: :model do
  it 'contains paper_trail' do
    is_expected.to be_versioned
  end
  
  context 'associations' do
    it { should belong_to(:category).with_foreign_key(:category_id) }
  end

  context 'validations' do
    it { should validate_presence_of(:value_pct) }
    it { should validate_numericality_of(:value_pct).is_less_than_or_equal_to(100).is_greater_than_or_equal_to(0) }
  end
end
