require 'rails_helper'

RSpec.describe PeriodConfiguration, type: :model do

  it 'contains paper_trail' do
    is_expected.to be_versioned
  end
  
  context 'associations' do
    it { should belong_to(:user).with_foreign_key(:user_id) }
  end

  it { should define_enum_for(:frequency).
              with_values(weekly: 1, monthly: 2) }
  
  context 'validations' do
    it { should validate_presence_of(:frequency) }
    it { should validate_presence_of(:num_frequency) }
    it { should validate_presence_of(:income_value_base) }

    let(:user) {
      create(:user)
    }


    it "can't allow num_frequency = 0" do
      period_configuration = build(:period_configuration, user: user, num_frequency: 0) 

      saved = period_configuration.save
      expect(saved).to be_falsey
      expect(period_configuration.errors[:num_frequency]).to be_present
    end

    it "can't allow income_value_base to be negative" do
      period_configuration = build(:period_configuration, user: user, income_value_base: -23.00) 

      saved = period_configuration.save
      expect(saved).to be_falsey
      expect(period_configuration.errors[:income_value_base]).to be_present
    end

    it 'should not save for invalid start_day' do
      invalid = build(:period_configuration, frequency: :monthly, start_day: 0, user: create(:user))

      expect(invalid.valid?).to be_falsey

      invalid = build(:period_configuration, frequency: :weekly, start_day: 0, user: create(:user))

      expect(invalid.valid?).to be_falsey
    end
  end

  it 'should save' do
    expect(create(:period_configuration).persisted?).to be_truthy
  end

end
