require 'rails_helper'

RSpec.describe RecurringExpense, type: :model do
  let(:user) do
    create(:user)
  end

  it 'contains paper_trail' do
    is_expected.to be_versioned
  end

  context 'associations' do
    it { should belong_to(:user).with_foreign_key(:user_id) }
    it { should belong_to(:category).with_foreign_key(:category_id).required(false) }
    it { should belong_to(:subcategory).with_foreign_key(:subcategory_id).required(false) }
    it { should belong_to(:payment_method).with_foreign_key(:payment_method_id).required(false) }

    it { should have_many(:expenses).with_foreign_key(:recurring_expense_id) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:value) }
    it { should validate_presence_of(:base_day) }
    it { should validate_presence_of(:start_date) }

    context 'subcategory is set' do
      let(:subcategory) do
        create(:subcategory)
      end

      it 'should have category set' do
        recurring_expense = build(:recurring_expense, category: nil, subcategory: subcategory)

        expect(recurring_expense.save).to be_falsey
        expect(recurring_expense.errors.key?(:category)).to be_truthy
      end

      it 'should have category of the subcategory equals to category' do
        recurring_expense = build(:recurring_expense, subcategory: subcategory)

        expect(recurring_expense.save).to be_falsey
        expect(recurring_expense.errors.key?(:category)).to be_truthy
      end
    end

    it 'doesnt allow end_date < start_date' do
      recurring_expense = build(:recurring_expense, category: nil, subcategory: nil, user: user,
                                                    start_date: Date.today, end_date: Date.today - 2.days)

      expect(recurring_expense.valid?).to be_falsey
      expect(recurring_expense.save).to be_falsey
      expect(recurring_expense.errors.key?(:end_date)).to be_truthy
    end
  end

  it 'saves' do
    expect(create(:recurring_expense).persisted?).to be_truthy
  end
end
