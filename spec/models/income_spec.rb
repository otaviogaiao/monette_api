require 'rails_helper'

RSpec.describe Income, type: :model do
  
  it 'contains paper_trail' do
    is_expected.to be_versioned
  end

  context 'associations' do
    it { should belong_to(:period).with_foreign_key(:period_id) }
    it { should belong_to(:user).with_foreign_key(:user_id) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:date_ocurred) }
    it { should validate_presence_of(:value) }

    it 'date_ocurred must be within period' do
      period = create(:period)

      income = build(:income, user: period.user, period: period, date_ocurred: period.start_date - 10.days)

      expect(income.save).to be_falsey
      expect(income.persisted?).to be_falsey
      expect(income.errors.key?(:date_ocurred)).to be_truthy
    end

    it 'doesnt allow date outside of period date range' do
      user = create(:user)
      period = create(:period, user: user, start_date: DateTime.current, end_date: DateTime.current + 1.month)

      income = build(:income, user: user, period: period, date_ocurred: period.start_date - 2.days)

      expect(income.valid?).to be_falsey
      expect(income.save).to be_falsey
      expect(income.errors.key?(:date_ocurred)).to be_truthy
    end
  end

  it 'saves' do
    income = create(:income)

    expect(income.persisted?).to be_truthy
  end
end
