require 'rails_helper'

RSpec.describe Installment, type: :model do
  it 'contains paper_trail' do
    is_expected.to be_versioned
  end
  
  context 'associations' do
    it { should belong_to(:user).with_foreign_key(:user_id) }
    it { should belong_to(:category).with_foreign_key(:category_id).required(false) }
    it { should belong_to(:subcategory).with_foreign_key(:subcategory_id).required(false) }

    it { should have_many(:expenses).with_foreign_key(:installment_id) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:total_value) }
    it { should validate_presence_of(:date_first_installment) }
    it { should validate_presence_of(:number_installments) }

    context 'subcategory is set' do
      let(:subcategory) do
        create(:subcategory)
      end

      it 'should have category set' do
        installment = build(:installment, category: nil, subcategory: subcategory)

        expect(installment.save).to be_falsey
        expect(installment.errors.key?(:category)).to be_truthy
      end

      it 'should have category of the subcategory equals to category' do
        installment = build(:installment, subcategory: subcategory)

        expect(installment.save).to be_falsey
        expect(installment.errors.key?(:category)).to be_truthy
      end
    end
  end
end
