module Api
  module V1
    class CategoriesController < ApplicationController
      before_action :authenticate_user!
      before_action :set_category, only: [:show, :update, :destroy]

      def index
        @categories = Category.includes(:subcategories)
                              .left_joins(:subcategories)
                              .where(user_id: current_user.id)
                              .where(deletion_date: nil)
                              .distinct
                              .order('categories.name ASC')
      end

      def create
        result = CreateCategory.result(user_id: current_user.id, category_params: category_params)

        if result.success?
          @category = result.category
          render :show, status: 201
        elsif result.status
          render_error(result)
        else
          render_validation_error(result.category)
        end
      end

      def update
        result = UpdateCategory.result(user_id: current_user.id, category_params: category_params, category: @category)

        if result.success?
          @category = result.category
          render :show, status: 200
        elsif result.status
          render_error(result)
        else
          render_validation_error(result.category)
        end
      end

      def show
      end

      def destroy
        result = DeleteCategory.result(category: @category, keep_history: params[:keep_history])

        if result.success?
          head :ok
        else
          render_validation_error(result.category)
        end
      end

      private

      def category_params
        params.permit(:name, :icon, :remove, subcategories_attributes: [:id, :name, :category_id, :remove])
      end

      def set_category
        @category = Category.includes(:subcategories)
                            .joins('LEFT OUTER JOIN subcategories ON subcategories.category_id = categories.id AND subcategories.deletion_date IS NULL')
                            .where(id: params[:id], user_id: current_user.id)
                            .distinct
                            .first

        raise ActiveRecord::RecordNotFound if @category.blank?
      end
    end
  end
end
