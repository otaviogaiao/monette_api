module Api
  module V1
    class PeriodsConfigurationsController < ApplicationController
      before_action :authenticate_user!
      before_action :set_period_configuration

      def index; end

      def update
        result = UpdatePeriodConfiguration.result(period_configuration: @period_configuration,
                                                       period_configuration_params: period_configuration_params)

        if result.success?
          @period_configuration = result.period_configuration
          render :index, status: 200
        else
          render_validation_error(result.period_configuration)
        end
      end

      private

      def set_period_configuration
        @period_configuration = PeriodConfiguration.where(user_id: current_user.id).first

        raise ActiveRecord::RecordNotFound if @period_configuration.blank?
      end

      def period_configuration_params
        params.permit(:frequency, :num_frequency, :income_value_base, :start_day)
      end
    end
  end
end
