module Api
  module V1
    class PaymentMethodsController < ApplicationController
      before_action :authenticate_user!

      before_action :set_payment_method, only: %i[show update destroy]

      def index
        @payment_methods = PaymentMethod.where(user_id: current_user.id).order(name: :asc)
      end

      def show; end

      def create
        result = CreatePaymentMethod.result(payment_method_params: payment_method_params, user_id: current_user.id)

        if result.success?
          @payment_method = result.payment_method
          render :show, status: 201
        elsif result.status
          render_error(result)
        else
          render_validation_error(result.payment_method)
        end
      end

      def update
        result = UpdatePaymentMethod.result(payment_method_params: payment_method_params,
                                            payment_method: @payment_method)

        if result.success?
          @payment_method = result.payment_method
          render :show, status: 200
        elsif result.status
          render_error(result)
        else
          render_validation_error(result.payment_method)
        end
      end

      def destroy
        result = DeletePaymentMethod.result(payment_method: @payment_method)

        if result.success?
          head :ok
        else
          render_validation_error(result.payment_method)
        end
      end

      private

      def set_payment_method
        @payment_method = PaymentMethod.where(user_id: current_user.id, id: params[:id]).first

        raise ActiveRecord::RecordNotFound if @payment_method.blank?
      end

      def payment_method_params
        params.permit(:name, :frequency, :start_day)
      end
    end
  end
end
