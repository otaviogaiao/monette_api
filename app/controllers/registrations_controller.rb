class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def create
    build_resource(sign_up_params)

    if resource.save
      render json: resource, status: 200
    else
      render_error(resource)
    end
  end

  private
  
  def sign_up_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
