class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  before_action :set_paper_trail_whodunnit

  def render_error(resource)
    render json: {
      error:
        {
          status: '400',
          title: 'Bad request',
          detail: resource.errors,
          code: '100'
        }
    }, status: 400
  end

  def render_validation_error(resource)
    render json: {
      error:
        {
          status: '422',
          title: 'Invalid data',
          detail: resource.errors,
          code: '100'
        }
    }, status: 422
  end

  def not_found
    render json: {
      errors: [
        {
          status: '404',
          title: 'Not found',
          detail: 'Resource not found',
          code: '404'
        }
      ]
    }, status: 404
  end
end
