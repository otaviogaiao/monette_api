class CompatibleCategorySubcategoryValidator < ActiveModel::Validator
  def validate(record)
    unless record.respond_to?(:category) && record.respond_to?(:subcategory)
      raise StandardError, "#{record.class.name} must contain associations category and subcategory"
    end

    # needs to be set
    record.errors.add(:category, :is_not_set) if record.subcategory.present? && record.category.blank?

    # should have category of the subcategory equals to category
    # needs to be compatible with subcategory
    if record.subcategory.present? && record.category.present? && record.category_id != record.subcategory.category_id
      record.errors.add(:category, :not_compatible)
    end
  end
end
