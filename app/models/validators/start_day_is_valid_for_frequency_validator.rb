class StartDayIsValidForFrequencyValidator < ActiveModel::Validator
  def validate(record)
    if !record.has_attribute?(:frequency) || !record.has_attribute?(:start_day)
      raise StandardError, "#{record.class.name} must contain attributes frequency and start_day"
    end

    if record.start_day.present? && record.frequency.present? &&
       ((record.weekly? && (record.start_day <= 0 || record.start_day >= 8)) ||
       (record.monthly? && (record.start_day <= 0 || record.start_day > 31)))
      record.errors.add(:start_day,
                        :invalid)
    end
  end
end
