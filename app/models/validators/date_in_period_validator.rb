class DateInPeriodValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    raise StandardError, "#{record.class.name} must contain association Period" unless record.respond_to?(:period)

    if record.period.present? && value.present? && (value < record.period.start_date || value > record.period.end_date)
      record.errors.add(attribute, :out_of_period)
    end
  end
end
