class StartDateEndDateValidator < ActiveModel::Validator
  def validate(record)
    unless record.has_attribute?(:start_date) && record.has_attribute?(:end_date)
      raise StandardError, "#{record.class.name} must contain attributes start_date and end_date"
    end

    if record.start_date.present? && record.end_date.present? &&
       record.start_date > record.end_date
      record.errors.add(:end_date,
                        :before_start_date)
    end
  end
end
