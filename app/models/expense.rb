class Expense < ApplicationRecord
  self.table_name = 'public.expenses'

  belongs_to :category, foreign_key: :category_id, required: false
  belongs_to :subcategory, foreign_key: :subcategory_id, required: false
  belongs_to :period, foreign_key: :period_id, required: false
  belongs_to :recurring_expense, foreign_key: :recurring_expense_id, required: false
  belongs_to :payment_method, foreign_key: :payment_method_id, required: false
  belongs_to :installment, foreign_key: :installment_id, required: false
  belongs_to :user, foreign_key: :user_id

  validates :name, :value, :date_ocurred, presence: true
  validates_with ::CompatibleCategorySubcategoryValidator
  validate :no_recurring_expense_and_installment
  validates :date_ocurred, date_in_period: true

  has_paper_trail

  private

  # doesnt allow both recurring expense and installment to be set
  def no_recurring_expense_and_installment
    errors.add(:base, :installment_and_recurring_expense_set) if installment.present? && recurring_expense.present?
  end
end
