class SpendingTargetConfiguration < ApplicationRecord
  self.table_name = 'public.spending_targets_configurations'

  belongs_to :category, foreign_key: :category_id

  validates :value_pct, presence: true,
                        numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }

  has_paper_trail
end
