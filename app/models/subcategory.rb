class Subcategory < ApplicationRecord
  self.table_name = 'public.subcategories'
  
  attr_accessor :remove

  belongs_to :category, foreign_key: :category_id, inverse_of: :subcategories
  has_many :recurring_expenses, foreign_key: :subcategory_id
  has_many :installments, foreign_key: :subcategory_id
  has_many :expenses, foreign_key: :subcategory_id

  validates :name, presence: true

  has_paper_trail

  before_save :update_deletion_date

  private

  def update_deletion_date
    self.deletion_date = Date.today if remove && deletion_date.blank?
  end
end
