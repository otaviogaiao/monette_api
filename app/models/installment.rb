class Installment < ApplicationRecord
  self.table_name = 'public.installments'

  belongs_to :category, foreign_key: :category_id, required: false
  belongs_to :subcategory, foreign_key: :subcategory_id, required: false
  belongs_to :user, foreign_key: :user_id

  has_many :expenses, foreign_key: :installment_id

  validates :name, :total_value, :date_first_installment, :number_installments, presence: true
  validates_with ::CompatibleCategorySubcategoryValidator

  has_paper_trail
end
