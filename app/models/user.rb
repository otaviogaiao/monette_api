class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :jwt_authenticatable, jwt_revocation_strategy: JwtDenylist

  self.table_name = 'public.users'

  has_many :categories, foreign_key: :user_id, dependent: :destroy
  has_many :periods_configurations, foreign_key: :user_id, dependent: :destroy
  has_many :periods, foreign_key: :user_id, dependent: :destroy
  has_many :incomes, foreign_key: :user_id, dependent: :destroy
  has_many :recurring_expenses, foreign_key: :user_id, dependent: :destroy
  has_many :payment_methods, foreign_key: :user_id, dependent: :destroy
  has_many :installments, foreign_key: :user_id, dependent: :destroy
  has_many :expenses, foreign_key: :user_id, dependent: :destroy

  validates :name, presence: true
  validates :email, presence: true, uniqueness: { case_sensitive: false } 

  has_paper_trail on: [:update]
end
