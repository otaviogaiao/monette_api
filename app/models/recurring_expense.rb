class RecurringExpense < ApplicationRecord
  self.table_name = 'public.recurring_expenses'

  belongs_to :user, foreign_key: :user_id
  belongs_to :category, foreign_key: :category_id, required: false
  belongs_to :subcategory, foreign_key: :subcategory_id, required: false
  belongs_to :payment_method, foreign_key: :payment_method_id, required: false

  has_many :expenses, foreign_key: :recurring_expense_id

  validates :name, :value, :base_day, :start_date, presence: true
  validates_with ::StartDateEndDateValidator
  validates_with ::CompatibleCategorySubcategoryValidator

  has_paper_trail
end
