class PaymentMethod < ApplicationRecord
  self.table_name = 'public.payment_methods'

  belongs_to :user, foreign_key: :user_id
  has_many :expenses, foreign_key: :payment_method_id, dependent: :nullify

  validates :name, presence: true
  validates :name, uniqueness: { scope: :user_id }
  validates_with ::StartDayIsValidForFrequencyValidator

  enum frequency: { weekly: 1, monthly: 2 }

  has_paper_trail
end
