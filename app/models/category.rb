class Category < ApplicationRecord
  self.table_name = 'public.categories'

  attr_accessor :remove

  belongs_to :user, foreign_key: :user_id
  has_many :subcategories, foreign_key: :category_id, inverse_of: :category, dependent: :destroy
  
  has_many :spending_targets, foreign_key: :category_id, dependent: :destroy
  has_many :recurring_expenses, foreign_key: :category_id, dependent: :nullify
  has_many :installments, foreign_key: :category_id, dependent: :nullify
  has_many :expenses, foreign_key: :category_id, dependent: :nullify

  has_one :spending_target_configuration, foreign_key: :category_id, dependent: :destroy

  accepts_nested_attributes_for :subcategories

  validates :name, presence: true

  has_paper_trail

  before_save :update_deletion_date

  private

  def update_deletion_date
    self.deletion_date = Date.today if remove && deletion_date.blank?
  end
end
