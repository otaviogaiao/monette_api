class SpendingTarget < ApplicationRecord
  self.table_name = 'public.spending_targets'

  belongs_to :category, foreign_key: :category_id
  belongs_to :period, foreign_key: :period_id

  validates :value_pct, presence: true,
                        numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }

  has_paper_trail
end
