class Income < ApplicationRecord
  self.table_name = "public.incomes"

  belongs_to :period, foreign_key: :period_id
  belongs_to :user, foreign_key: :user_id

  validates :name, :date_ocurred, :value, presence: true
  validate :cant_ocurr_outside_period
  validates :date_ocurred, date_in_period: true

  has_paper_trail

  private

  def cant_ocurr_outside_period
    if period.present? && date_ocurred.present? && (date_ocurred < period.start_date || date_ocurred > period.end_date)
      errors.add(:date_ocurred, :outside_of_period)
    end
  end
end
