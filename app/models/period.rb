class Period < ApplicationRecord
  self.table_name = 'public.periods'

  belongs_to :user, foreign_key: :user_id

  has_many :incomes, foreign_key: :period_id
  has_many :expenses, foreign_key: :period_id

  validates :start_date, :end_date, :income_value, presence: true
  validates_with ::StartDateEndDateValidator

  has_paper_trail
end
