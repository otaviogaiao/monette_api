class PeriodConfiguration < ApplicationRecord
  self.table_name = 'public.periods_configurations'

  belongs_to :user, foreign_key: :user_id

  enum frequency: { weekly: 1, monthly: 2 }, _default: :monthly

  validates :frequency, :num_frequency, :income_value_base, presence: true
  validate :num_frequency_cant_be_zero, if: -> { !num_frequency.nil? }
  validate :income_value_base_cant_be_negative, if: -> { !income_value_base.nil? }
  validates_with ::StartDayIsValidForFrequencyValidator

  has_paper_trail

  private

  def num_frequency_cant_be_zero
    if num_frequency&.zero?
      errors.add(:num_frequency, :equal_to_zero) # can't be equal to zero
    end
  end

  def income_value_base_cant_be_negative
    if income_value_base.present? && income_value_base < 0
      errors.add(:income_value_base, :is_negative) #can't be negative
    end
  end
end
