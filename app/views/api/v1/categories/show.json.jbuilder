json.call(@category, :id, :name, :icon)
json.subcategories @category.subcategories.where(deletion_date: nil) do |subcategory|
  json.call(subcategory, :id, :name, :category_id)
end