json.call(@period_configuration, :id, :frequency, :num_frequency, :start_day)
json.frequency_label PeriodConfiguration.human_attribute_name("frequency.#{@period_configuration.frequency}")
json.income_value_base @period_configuration.income_value_base&.to_f