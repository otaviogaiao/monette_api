json.array! @payment_methods do |payment_method|
  json.call(payment_method, :id, :name, :frequency, :start_day)
  json.frequency_label PaymentMethod.human_attribute_name("frequency.#{payment_method.frequency}")
end
