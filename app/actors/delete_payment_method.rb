class DeletePaymentMethod < Actor
  input :payment_method

  def call
    fail! unless payment_method.destroy
  end
end
