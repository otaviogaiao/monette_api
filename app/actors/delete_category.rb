class DeleteCategory < Actor
  input :category
  input :keep_history, default: true

  def call
    # When a user removes a category or subcategory, he can choose to keep history for old periods, in this case,
    # the field deletion_date is filled, so this categories wont be showed for periods with date after deletion_date.
    # If he opts for not keeping history, the category and its subcategories are deleted and the connected registers are nullifed or destroyed
    ActiveRecord::Base.transaction do
      if ActiveModel::Type::Boolean.new.cast(keep_history)
        category.deletion_date = Date.today
        category.save!

        category.subcategories.update_all(deletion_date: Date.today)
      else
        fail! unless category.destroy
      end
    end
  end
end
