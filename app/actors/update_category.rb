class UpdateCategory < Actor
  input :category_params, allow_nil: false
  input :category, type: Category

  def call
    category.assign_attributes(category_params)

    fail! unless category.save
  end
end
