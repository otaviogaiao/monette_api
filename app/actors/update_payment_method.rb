class UpdatePaymentMethod < Actor
  input :payment_method_params, allow_nil: false
  input :payment_method, type: PaymentMethod

  def call
    payment_method.assign_attributes(payment_method_params)

    fail! unless payment_method.save
  end
end
