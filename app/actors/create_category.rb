class CreateCategory < Actor
  input :user_id, allow_nil: false
  input :category_params, allow_nil: false
  output :category

  def call
    self.category = Category.new(category_params.merge({ user_id: user_id }))

    fail! unless self.category.save
  end
end
