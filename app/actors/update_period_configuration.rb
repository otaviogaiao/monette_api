class UpdatePeriodConfiguration < Actor
  input :period_configuration, type: PeriodConfiguration, allow_nil: false
  input :period_configuration_params

  def call
    period_configuration.assign_attributes(period_configuration_params)
    fail! unless period_configuration.save
  end
end
