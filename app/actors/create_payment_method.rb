class CreatePaymentMethod < Actor
  input :payment_method_params, allow_nil: false
  input :user_id
  output :payment_method

  def call
    fail!(errors: "Only up to 50 payment methods are allowed", status: 400) if PaymentMethod.where(user_id: user_id).count >= 50

    self.payment_method = PaymentMethod.new(payment_method_params.merge({user_id: user_id}))

    fail! unless payment_method.save
  end
end
