# Docker

## Executar apenas esse container

### Utilizando docker

Constroi imagem:

```
docker build -t monette_api -f Dockerfile.dev .
```

Cria container e executa a partir da imagem criada no passo anterior:

```
docker run -p 8080:8080 -v $(pwd):/apps/monette_api -it monette_api bash
```

O comando anterior vai criar o container e abrir o terminal dele. Caso queira já iniciar o servidor rails, só usar:

```
docker run -p 8080:8080 -v $(pwd):/apps/gaia_vue -it monette_api
```

Caso queira criar um container, e inicia-lo depois:

```
docker build -t monette_api -f Dockerfile.dev .
docker create -v $(pwd):/apps/monette_api -it -p 8080:8080 monette_api
docker start -i f36d4d9044b08e42b2b9ec1b02b03b86b3ae7da243f5268db2180f3194823e48
```

O comando create gera um hash id, que é usado no comando start.

### Verificando containers rodando

```
docker ps
```

### Parar containers

```
docker stop <container-id>
```

ou, mais agressivo

```
docker kill <container-id>
```

### Utilizando docker-compose

Executa container rodando o bash

```
docker-compose run --service-ports --rm monette_api bash
```

Sobe container já com o servidor yarn rodando:

```
docker-compose up
```

Para parar o container:

```
docker-compose down
```

## Para abrir mais terminais em um container que já esteja rodando

```
docker exec -it <container-id> sh
```
