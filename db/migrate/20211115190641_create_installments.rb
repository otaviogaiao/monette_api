class CreateInstallments < ActiveRecord::Migration[6.1]
  def change
    create_table :installments do |t|
      t.string :name, null: false
      t.references :category, foreign_key: true
      t.references :subcategory, foreign_key: true
      t.decimal :total_value, null: false
      t.datetime :date_first_installment, null: false
      t.integer :number_installments, null: false
      t.float :interest_rate, default: 0
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
