class CreateCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :categories do |t|
      t.string :name, null: false
      t.string :icon
      t.references :user, null: false, foreign_key: {to_table: :users}
      t.datetime :deletion_date

      t.timestamps
    end
  end
end
