class CreateRecurringExpenses < ActiveRecord::Migration[6.1]
  def change
    create_table :recurring_expenses do |t|
      t.string :name, null: false
      t.decimal :value, null: false
      t.integer :base_day, null: false
      t.references :user, null: false, foreign_key: true
      t.date :start_date, null: false
      t.date :end_date
      t.references :category, foreign_key: true
      t.references :subcategory, foreign_key: true
      t.references :payment_method, foreign_key: true

      t.timestamps
    end
  end
end
