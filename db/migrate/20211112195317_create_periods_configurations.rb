class CreatePeriodsConfigurations < ActiveRecord::Migration[6.1]
  def change
    create_table :periods_configurations do |t|
      t.integer :frequency, null: false
      t.integer :num_frequency, null: false
      t.references :user, null: false, foreign_key: true
      t.decimal :income_value_base, null: false, precision: 12, scale: 2

      t.timestamps
    end
  end
end
