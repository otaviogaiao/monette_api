class CreateSpendingTargetsConfigurations < ActiveRecord::Migration[6.1]
  def change
    create_table :spending_targets_configurations do |t|
      t.float :value_pct, null: false
      t.references :category, null: false, foreign_key: true

      t.timestamps
    end
  end
end
