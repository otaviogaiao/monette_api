class AlterPeriodConfigurationAndPaymentMethod < ActiveRecord::Migration[6.1]
  def up
    add_column :periods_configurations, :start_day, :integer, default: 2
    add_column :payment_methods, :start_day, :integer, null: false
    add_column :payment_methods, :frequency, :integer, null: false
  end

  def down
    remove_column :periods_configurations, :start_day
    remove_column :payment_methods, :start_day
    remove_column :payment_methods, :frequency
  end
end
