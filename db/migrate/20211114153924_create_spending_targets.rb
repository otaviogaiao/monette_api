class CreateSpendingTargets < ActiveRecord::Migration[6.1]
  def change
    create_table :spending_targets do |t|
      t.float :value_pct, null: false
      t.references :category, null: false, foreign_key: true
      t.references :period, null: false, foreign_key: true

      t.timestamps
    end
  end
end
