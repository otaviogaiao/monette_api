class CreateExpenses < ActiveRecord::Migration[6.1]
  def change
    create_table :expenses do |t|
      t.string :name, null: false
      t.references :category, foreign_key: true
      t.references :subcategory, foreign_key: true
      t.decimal :value, null: false
      t.date :date_ocurred, null: false
      t.references :period, foreign_key: true
      t.references :recurring_expense, foreign_key: true
      t.references :payment_method, foreign_key: true
      t.references :installment, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
