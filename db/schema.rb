# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_22_200108) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name", null: false
    t.string "icon"
    t.bigint "user_id", null: false
    t.datetime "deletion_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_categories_on_user_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "category_id"
    t.bigint "subcategory_id"
    t.decimal "value", null: false
    t.date "date_ocurred", null: false
    t.bigint "period_id"
    t.bigint "recurring_expense_id"
    t.bigint "payment_method_id"
    t.bigint "installment_id"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_expenses_on_category_id"
    t.index ["installment_id"], name: "index_expenses_on_installment_id"
    t.index ["payment_method_id"], name: "index_expenses_on_payment_method_id"
    t.index ["period_id"], name: "index_expenses_on_period_id"
    t.index ["recurring_expense_id"], name: "index_expenses_on_recurring_expense_id"
    t.index ["subcategory_id"], name: "index_expenses_on_subcategory_id"
    t.index ["user_id"], name: "index_expenses_on_user_id"
  end

  create_table "incomes", force: :cascade do |t|
    t.string "name", null: false
    t.date "date_ocurred", null: false
    t.decimal "value", null: false
    t.bigint "period_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["period_id"], name: "index_incomes_on_period_id"
    t.index ["user_id"], name: "index_incomes_on_user_id"
  end

  create_table "installments", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "category_id"
    t.bigint "subcategory_id"
    t.decimal "total_value", null: false
    t.datetime "date_first_installment", null: false
    t.integer "number_installments", null: false
    t.float "interest_rate", default: 0.0
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_installments_on_category_id"
    t.index ["subcategory_id"], name: "index_installments_on_subcategory_id"
    t.index ["user_id"], name: "index_installments_on_user_id"
  end

  create_table "jwt_denylist", force: :cascade do |t|
    t.string "jti", null: false
    t.datetime "exp", null: false
    t.index ["jti"], name: "index_jwt_denylist_on_jti"
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "start_day", null: false
    t.integer "frequency", null: false
    t.index ["user_id"], name: "index_payment_methods_on_user_id"
  end

  create_table "periods", force: :cascade do |t|
    t.date "start_date", null: false
    t.date "end_date", null: false
    t.bigint "user_id", null: false
    t.decimal "income_value", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_periods_on_user_id"
  end

  create_table "periods_configurations", force: :cascade do |t|
    t.integer "frequency", null: false
    t.integer "num_frequency", null: false
    t.bigint "user_id", null: false
    t.decimal "income_value_base", precision: 12, scale: 2, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "start_day", default: 2
    t.index ["user_id"], name: "index_periods_configurations_on_user_id"
  end

  create_table "recurring_expenses", force: :cascade do |t|
    t.string "name", null: false
    t.decimal "value", null: false
    t.integer "base_day", null: false
    t.bigint "user_id", null: false
    t.date "start_date", null: false
    t.date "end_date"
    t.bigint "category_id"
    t.bigint "subcategory_id"
    t.bigint "payment_method_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_recurring_expenses_on_category_id"
    t.index ["payment_method_id"], name: "index_recurring_expenses_on_payment_method_id"
    t.index ["subcategory_id"], name: "index_recurring_expenses_on_subcategory_id"
    t.index ["user_id"], name: "index_recurring_expenses_on_user_id"
  end

  create_table "spending_targets", force: :cascade do |t|
    t.float "value_pct", null: false
    t.bigint "category_id", null: false
    t.bigint "period_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_spending_targets_on_category_id"
    t.index ["period_id"], name: "index_spending_targets_on_period_id"
  end

  create_table "spending_targets_configurations", force: :cascade do |t|
    t.float "value_pct", null: false
    t.bigint "category_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_spending_targets_configurations_on_category_id"
  end

  create_table "subcategories", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "category_id", null: false
    t.datetime "deletion_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["category_id"], name: "index_subcategories_on_category_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type"
    t.string "{:null=>false}"
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "categories", "users"
  add_foreign_key "expenses", "categories"
  add_foreign_key "expenses", "installments"
  add_foreign_key "expenses", "payment_methods"
  add_foreign_key "expenses", "periods"
  add_foreign_key "expenses", "recurring_expenses"
  add_foreign_key "expenses", "subcategories"
  add_foreign_key "expenses", "users"
  add_foreign_key "incomes", "periods"
  add_foreign_key "incomes", "users"
  add_foreign_key "installments", "categories"
  add_foreign_key "installments", "subcategories"
  add_foreign_key "installments", "users"
  add_foreign_key "payment_methods", "users"
  add_foreign_key "periods", "users"
  add_foreign_key "periods_configurations", "users"
  add_foreign_key "recurring_expenses", "categories"
  add_foreign_key "recurring_expenses", "payment_methods"
  add_foreign_key "recurring_expenses", "subcategories"
  add_foreign_key "recurring_expenses", "users"
  add_foreign_key "spending_targets", "categories"
  add_foreign_key "spending_targets", "periods"
  add_foreign_key "spending_targets_configurations", "categories"
  add_foreign_key "subcategories", "categories"
end
