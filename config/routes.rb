Rails.application.routes.draw do
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'api/login',
               sign_out: 'api/logout',
               registration: 'api/signup'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  defaults format: :json do
    namespace :api do
      namespace :v1 do
        scope :periods_configurations, as: :period_configuration do
          get '/', to: 'periods_configurations#index'
          put '/', to: 'periods_configurations#update'
          patch '/', to: 'periods_configurations#update'
        end

        resources :payment_methods
        resources :categories
      end
    end
  end
end
